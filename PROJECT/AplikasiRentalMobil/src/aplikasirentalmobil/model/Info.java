/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasirentalmobil.model;

/**
 *
 * @author LENOVO
 */
public class Info {
    private final String aplikasi = "Rental Mobil Jaya";
    private final String greeting = "Hallo, Selamat Datang...";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getGreeting() {
        return greeting;
    }
}
