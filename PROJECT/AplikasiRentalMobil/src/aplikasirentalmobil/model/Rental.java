package aplikasirentalmobil.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author LENOVO
 */
public class Rental implements Serializable {

    private static final long serialVersionUID = -6756463875294313469L;
    private int noTransaski;
    private int jenisMobil;
    private String noPlat;
    private String noSTNK;
    private int lamaSewa;
    private String NoKTPPenyewa;
    private LocalDateTime waktuMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private boolean keluar = false;

    public Rental() {
    }

    public Rental(int noTransaksi, int jenisMobil, String noPlat, String noSTNK, int lamaSewa, String NoKTPPenyewa, LocalDateTime waktuMasuk, LocalDateTime waktuKeluar, BigDecimal biaya) {
        this.noTransaski = noTransaksi;
        this.jenisMobil = jenisMobil;
        this.noPlat = noPlat;
        this.noSTNK = noSTNK;
        this.lamaSewa = lamaSewa;
        this.NoKTPPenyewa = NoKTPPenyewa;
        this.waktuMasuk = waktuMasuk;
        this.waktuKeluar = waktuKeluar;
        this.biaya = biaya;

    }

    public int getNoTransaski() {
        return noTransaski;
    }

    public void setNoTransaski(int noTransaski) {
        this.noTransaski = noTransaski;
    }

    public int getJenisMobil() {
        return jenisMobil;
    }

    public void setJenisMobil(int jenisMobil) {
        this.jenisMobil = jenisMobil;
    }

    public String getNoPlat() {
        return noPlat;
    }

    public void setNoPlat(String noPlat) {
        this.noPlat = noPlat;
    }

    public String getNoSTNK() {
        return noSTNK;
    }

    public void setNoSTNK(String noSTNK) {
        this.noSTNK = noSTNK;
    }

    public int getLamaSewa() {
        return lamaSewa;
    }

    public void setLamaSewa(int lamaSewa) {
        this.lamaSewa = lamaSewa;
    }

    public String getNoKTPPenyewa() {
        return NoKTPPenyewa;
    }

    public void setNoKTPPenyewa(String NoKTPPenyewa) {
        this.NoKTPPenyewa = NoKTPPenyewa;
    }

    public LocalDateTime getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(LocalDateTime waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public LocalDateTime getWaktuKeluar() {
        return waktuKeluar;
    }

    public void setWaktuKeluar(LocalDateTime waktuKeluar) {
        this.waktuKeluar = waktuKeluar;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    @Override
    public String toString() {
        return "Rental{" + "noTransaski=" + noTransaski + ", jenisMobil=" + jenisMobil + ", noPlat=" + noPlat
                + ", noSTNK=" + noSTNK + ", lamaSewa=" + lamaSewa + ", NoKTPPenyewa=" + NoKTPPenyewa 
                + ", waktuMasuk=" + waktuMasuk + ", waktuKeluar=" + waktuKeluar + ", biaya=" + biaya + ", keluar=" + keluar + '}';
    }

}
