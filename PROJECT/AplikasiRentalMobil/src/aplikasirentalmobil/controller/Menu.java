package aplikasirentalmobil.controller;

import aplikasirentalmobil.model.Info;
import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getGreeting());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Rental");
        System.out.println("2. Menu Pengembalian");
        System.out.println("3. Laporan");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        RentalController i = new RentalController();
        switch (noMenu) {
            case 1:
                i.setMenuRental();
                break;
            case 2:
                i.setMenuPengembalian();
                break;
            case 3:
                i.getLaporanRental();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}
