package aplikasirentalmobil.controller;

import aplikasirentalmobil.model.Rental;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author LENOVO
 */
public class RentalController {

    private static final String FILE = "D:\\rental.json";
    private Rental rental;
    private final Scanner in;

    private final DateTimeFormatter dateTimeFormat;
    private int noTransaksi;
    private int jenisMobil;
    private String noPlat;
    private String noSTNK;
    private int lamaSewa;
    private String NoKTPPenyewa;
    private LocalDateTime waktuMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private boolean keluar = false;
    private int pilihan;

    public RentalController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        waktuKeluar = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * Method untuk memilih jenis mobil, identitas mobil, identitas penyewa
     */
    public void setMenuRental() {
        System.out.print("No transaksi : ");
        noTransaksi = in.nextInt();
        System.out.println("Jenis Kendaraan 1 = Mobil L\t 2 = Mobil XL");
        System.out.print("Masukkan Jenis Mobil : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
        }
        jenisMobil = in.nextInt();
        rental = new Rental();

        System.out.println("===Masukan Identitas Mobil===");
        System.out.print("Masukan plat nomor mobil : ");
        noPlat = in.next();
        System.out.print("Masukan no STNK mobil : ");
        noSTNK = in.next();
        System.out.print("Masukan lama penyewaan : ");
        lamaSewa = in.nextInt();

        System.out.println("===Masukan Identitas Penyewa===");
        System.out.print("Masukan no ktp penyewa : ");
        NoKTPPenyewa = in.next();

        String formatWaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.println("Waktu mobil disewa : " + formatWaktuMasuk);

        rental.setNoTransaski(noTransaksi);
        rental.setJenisMobil(jenisMobil);
        rental.setNoPlat(noPlat);
        rental.setNoSTNK(noSTNK);
        rental.setLamaSewa(lamaSewa);
        rental.setNoKTPPenyewa(NoKTPPenyewa);
        rental.setWaktuMasuk(waktuMasuk);
        setWriteRental(FILE, rental);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu o = new Menu();
            o.getMenuAwal();
        } else {
            setMenuRental();
        }
    }

    public List<Rental> getReadRental(String file) {
        List<Rental> apprental = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader r = new FileReader(file)) {
            BufferedReader br = new BufferedReader(r);
            while ((line = br.readLine()) != null) {
                Rental[] ps = gson.fromJson(line, Rental[].class);
                apprental.addAll(Arrays.asList(ps));
            }
            br.close();
            r.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RentalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RentalController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return apprental;
    }

    public void setWriteRental(String file, Rental rental) {
        Gson gson = new Gson();

        List<Rental> apprental = getReadRental(file);
        apprental.remove(rental);
        apprental.add(rental);

        String json = gson.toJson(apprental);
        try {
            try (FileWriter writer = new FileWriter(file)) {
                writer.write(json);
            }
        } catch (IOException ex) {
            Logger.getLogger(RentalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Rental getSearch(int noTransaksi) {
        List<Rental> apprental = getReadRental(FILE);

        Rental p = apprental.stream()
                .filter(pp -> noTransaksi == pp.getNoTransaski())
                .findAny()
                .orElse(null);

        return p;
    }

    public void setMenuPengembalian() {
        System.out.print("Masukan no transaksi : ");
        noTransaksi = in.nextInt();

        Rental p = getSearch(noTransaksi);
        if (p != null) {

            waktuKeluar = LocalDateTime.now();

            p.setWaktuKeluar(waktuKeluar);

            if (p.getLamaSewa() > 0) {
                biaya = new BigDecimal(p.getLamaSewa());
                if (p.getJenisMobil() == 1) {
                    biaya = biaya.multiply(new BigDecimal(300000));
                } else {
                    biaya = biaya.multiply(new BigDecimal(350000));
                }
            }

            p.setBiaya(biaya);
            p.setKeluar(true);

            System.out.println("No Polisi : " + p.getNoPlat());
            System.out.println("Jenis : " + (p.getJenisMobil() == 1 ? "Mobil L" : "Mobil XL"));
            System.out.println("Waktu Masuk : " + p.getWaktuMasuk().format(dateTimeFormat));
            System.out.println("Waktu Keluar : " + p.getWaktuKeluar().format(dateTimeFormat));
            System.out.println("Lama : " + p.getLamaSewa() + " hari");
            System.out.println("Biaya : " + biaya);

            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();

            switch (pilihan) {
                case 1:
                    setWriteRental(FILE, p);
                    break;
                case 2:
                    setMenuPengembalian();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setMenuPengembalian();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setMenuPengembalian();
        }
    }

    /**
     * Method untuk menampilkan data parkir yang sudah keluar
     */
    public void getLaporanRental() {
        List<Rental> r = getReadRental(FILE);
        Predicate<Rental> isKeluar = e -> e.isKeluar() == true;
        Predicate<Rental> isDate = e -> e.getWaktuKeluar().toLocalDate().equals(LocalDate.now());

        List<Rental> pResults = r.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(Rental::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("No Plat \tWaktu Masuk \t\tLama Penyewaan(hari) \t\tWaktu Keluar \t\tBiaya");
        System.out.println("---------\t-------------------\t-----------------\t\t------------------\t------------------ ");
        pResults.forEach((p) -> {
            System.out.println(p.getNoPlat() + "\t\t" + p.getWaktuMasuk().format(dateTimeFormat)
                    + "\t\t" + p.getLamaSewa() + "\t\t\t" + p.getWaktuKeluar().format(dateTimeFormat)
                    + "\t" + p.getBiaya());
        });
        System.out.println("---------\t-------------------\t-----------------\t\t------------------\t------------------ ");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getLaporanRental();
        }
    }
}
