 package id.aghnisyifahmari.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author LENOVO
 */
public class ShellSort {

    public static int[] getShellSort(int L[], int n) {
        int step, start, i, j, y;
        boolean ketemu;

        step = L.length;
        while (step > 1) {
            step = ((step / 3) + 1);
            for (start = 0; start <= step; start++) {
                i = (start + step);
                while (i <= n-1) {
                    y = L[i];
                    j = i - step;
                    ketemu = false;
                    System.out.println("i :" + i + ", j : " + j + " ---> " + L[j]); 
                    while ((j >= 0) && (!ketemu)) {
                        if (y < L[j]) {
                            L[j + step] = L[j];
                            j = j - step;
                        } else {
                            ketemu = true;
                        }
                    }
                    L[j + step] = y;
                    i = i + step;
                }
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = 6;
        System.out.println("===ShellSort===");
        System.out.println(Arrays.toString(L));
        getShellSort(L, n);
        System.out.println(Arrays.toString(L));
    }
}
