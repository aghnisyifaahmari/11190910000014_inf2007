package id.aghnisyifahmari.pertemuan.kesepuluh;
import java.util.Arrays;
/**
 *
 * @author LENOVO
 */
public class PengurutanApung {
       public static int[] getBubbleSort(int L[], int n) {
        int i, k, temp;
    
        for (i = n-1; i > 0 ; i--) {
            for (k = 0; k < i; k++) {
                System.out.println("i :" + i + ", k : " + (k + 1) + " ---> " + L[k + 1]);
                if (L[k] < L[k + 1]) {
                    temp = L[k];
                    L[k] = L[k + 1];
                    L[k + 1] = temp;
                }
            }
        }
        return L;
    }
       public static void main(String[] args) {
           
           int L[] = {12, 11, 15, 63, 24};
           int n = L.length;
           System.out.println("===Pengurutan Apung===");
           System.out.println(Arrays.toString(L));
           getBubbleSort(L, n);
           System.out.println(Arrays.toString(L));
           
                   
    }
}
