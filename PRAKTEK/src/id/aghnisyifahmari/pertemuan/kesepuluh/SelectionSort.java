package id.aghnisyifahmari.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author LENOVO
 */
public class SelectionSort {

    public static int[] getSelectionSortMax(int L[], int n) {
        int i, j, imaks, temp;
        for (i = n - 1; i > 1; i--) {
            imaks = 0;
            for (j = 1; j < i; j++) {
                System.out.println("i :" + i + ", j : " + j + " ---> " + L[j]);
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }

    public static int[] getSelectionSortMin(int L[], int n) {
        int i, j, imin, temp;
        for (i = 0; i < n - 1; i++) {
            imin = i;
            for (j = i; j < n; j++) {
                System.out.println("i :" + i + ", j : " + j + " ---> " + L[j]);
                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }

    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = 6;
        System.out.println("===SelectionSort===");
//        System.out.println(Arrays.toString(L));
//        getSelectionSortMax(L, n);
        System.out.println(Arrays.toString(L));
        getSelectionSortMin(L, n);
        System.out.println(Arrays.toString(L));
    }
}
