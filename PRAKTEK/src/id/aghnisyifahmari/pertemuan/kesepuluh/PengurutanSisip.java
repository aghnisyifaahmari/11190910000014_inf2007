package id.aghnisyifahmari.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class PengurutanSisip {

    public static int[] InsertionSort(int L[], int n) {
        Scanner in = new Scanner(System.in);
        int i, j, y, k;
        boolean ketemu;
        System.out.println("Masukan nilai larik : ");
        for (k = n - 1; k >= 0; k--) {
            L[k] = in.nextInt();
            for (i = 1; i < n; i++) {
                y = L[i];
                j = i - 1;
                ketemu = false;
                while ((j >= 0) && (!ketemu)) {
                    if (y < L[j]) {
                        L[j + 1] = L[j];
                        j = j - 1;
                    } else {
                        ketemu = true;
                    }
                }
                L[j + 1] = y;
            }
            System.out.println(Arrays.toString(L));
        }
        return L;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        System.out.print("Masukan jumlah nilai larik : ");
        n = in.nextInt();
        int L[] = new int[n];

        System.out.println("===InsertionSort===");
        
        System.out.println(Arrays.toString(L));
        InsertionSort(L, n);
    }
}
