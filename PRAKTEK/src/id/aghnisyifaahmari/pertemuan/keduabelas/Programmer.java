package id.aghnisyifaahmari.pertemuan.keduabelas;

/**
 *
 * @author LENOVO
 */
public class Programmer extends Pegawai {

    private int bonus;

    public Programmer(String nama, int gaji, int Bonus) {
        super(nama, gaji);
        this.bonus = bonus;

    }

    public int infoGaji() {
        return this.gaji;
    }

    public int infoBonus() {
        return this.bonus;
    }
}
