package id.aghnisyifaahmari.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class AplikasiMatriks {

    public static void main(String[] args) {
        int i, j, Baris, Kolom;
        Matriks matriks = new Matriks();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan jumlah baris : ");
        Baris = in.nextInt();
        System.out.println("Masukan jumlah kolom : ");
        Kolom = in.nextInt();
        int[][] Array1 = new int[Baris][Kolom];
        int[][] Array2 = new int[Baris][Kolom];

           for (i = 0; i < Baris; i++) {
            System.out.println("Masukan nilai larik 1 baris ke " + (i + 1));
            for (j = 0; j < Kolom; j++) {
                Array1[i][j] = in.nextInt();
            }
        }

        for (i = 0; i < Baris; i++) {
            System.out.println("Masukan nilai larik 2 baris ke " + (i + 1));
            for (j = 0; j < Kolom; j++) {
                Array2[i][j] = in.nextInt();
            }
        }
        
        System.out.println("Matriks ke 1");
        for (i = 0; i < Baris; i++) {
            System.out.print("[");
            for (j = 0; j < Kolom; j++) {
                System.out.print(" " + Array1[i][j] + " ");
            }
            System.out.println("]");
        }

        System.out.println("Matriks ke 2");
        for (i = 0; i < Baris; i++) {
            System.out.print("[");
            for (j = 0; j < Kolom; j++) {
                System.out.print(" " + Array2[i][j] + " ");
            }
            System.out.println("]");
        }


            System.out.println("Matriks hasil penjumlahan");
            matriks.getPenambahanMatriks(Array1, Array2, Baris, Kolom);
        }
    }

