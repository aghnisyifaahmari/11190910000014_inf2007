package id.aghnisyifaahmari.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author LENOVO
 */
public class InputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        System.out.println("Bilangan: "+ bilangan);
    }
    
}
