
package id.aghnisyifaahmari.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author LENOVO
 */
public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null,"Bilangan: "+ bilangan,
                "Hasil Input", JOptionPane.INFORMATION_MESSAGE);
    }
    
}
