package id.aghnisyifaahmari.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class Segitiga {

    double alas, tinggi, luas;

    public Segitiga() {
        Scanner in = new Scanner(System.in);
        System.out.println("==Luas Segitiga==");
        System.out.println("Masukan alas = ");
        alas = in.nextDouble();
        System.out.println("Masukan tinggi = ");
        tinggi = in.nextDouble();
        System.out.println("Luasnya = ");
        luas = (alas * tinggi)/2;
        System.out.println(luas);
        
    }

}
