package id.aghnisyifaahmari.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class AplikasiSegitigaParameter {

    public static void main(String[] args) {
        int i, N;
        float a, t;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan banyaknya segitiga = ");
        N = in.nextInt();

        for (i = 1; i <= N; i++) {
            System.out.println("== Luas Segitiga ==");
            System.out.println("Masukan nilai alas = ");
            a = in.nextFloat();
            System.out.println("Masukan nilai tinggi = ");
            t = in.nextFloat();
            SegitigaParameter hitungLuasSegitiga = new SegitigaParameter(a, t);
            System.out.println("Luas = ");
            hitungLuasSegitiga.getLuas();
        }
    }

}
