package id.aghnisyifaahmari.pertemuan.ketujuh;

/**
 *
 * @author LENOVO
 */
public class AplikasiFungsi {

    public static void main(String[] args) {
        float x;
        Fungsi hasilFungsi = new Fungsi();

        System.out.println("------------------------");
        System.out.println(" x\t\tf (x)  ");
        System.out.println("------------------------");

        x = (float) 10.0;
        while (x <= 15.0) {
            x = (x +(float) 0.2);
            System.out.println(x + "\t\t" + hasilFungsi.getFungsi(x));
        }
        System.out.println("----------------------------");
    }
}
