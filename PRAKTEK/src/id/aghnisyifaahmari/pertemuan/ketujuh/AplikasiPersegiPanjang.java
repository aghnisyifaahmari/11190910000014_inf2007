package id.aghnisyifaahmari.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class AplikasiPersegiPanjang {
    public static void main(String[] args) {
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();
        
        Scanner in = new Scanner(System.in);
        double panjang, lebar;
        
        System.out.println("Masukan panjang = ");
        panjang = in.nextDouble();
        
        System.out.println("Masukan lebar = ");
        lebar = in.nextDouble();
        
        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getInfo();
        
        System.out.println("Luas = "+ persegiPanjang.getLuas());
        System.out.println("Keliling = "+ persegiPanjang.getKeliling());
            
        }
    }

