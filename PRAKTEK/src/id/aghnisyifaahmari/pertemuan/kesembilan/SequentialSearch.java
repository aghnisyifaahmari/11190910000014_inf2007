package id.aghnisyifaahmari.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class SequentialSearch {

    public boolean getSearchOutBoolean(int[] L, int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke- " + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke- " + i + " isinya " + L[i]);
        }
        return L[i] == x;
    }

    public int getSearchOutIndeks(int[] L, int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke- " + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke- " + i + " isinya " + L[i]);
        }
        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }

    public boolean getSearchInBoolean(int[] L, int n, int x) {
        int i = 0;
        Boolean ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke - " + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;

                System.out.println("Posisi ke - " + i + " isinya " + L[i]);
            }
        }
        return ketemu;
    }

    public int getSearchInIndeks(int[] L, int n, int x) {
        int i = 0;
        Boolean ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke - " + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;

                System.out.println("Posisi ke - " + i + " isinya " + L[i]);
            }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public int getSearchSentinel(int[] L, int n, int x) {
        int i = 0, idx;
        L[n + 1] = x;
        while (L[i] != x) {
            i = i + 1;
        }
        if (i == n + 1) {
            return idx = -1;
        } else {
            return idx = i;
        }
    }

    public static void main(String[] args) {
        int i, x, n = 6;
        Scanner in = new Scanner(System.in);
        int L[] = new int[7];
        SequentialSearch app = new SequentialSearch();
        System.out.print("Masukan nilai x = ");
        x = in.nextInt();
        L [0] = 13;
        L [1] = 16;
        L [2] = 14;
        L [3] = 21;
        L [4] = 76;
        L [5] = 15;
        n = L.length - 2;
        System.out.println("Indeks ke "+ app.getSearchSentinel(L, n, x));
//       int[] L = new int[]{13, 16, 14, 21, 76, 15};
//        System.out.println("indeks = " + app.getSearchOutBoolean(L, n, x));
//        System.out.println("data ada di indeks ke = " + app.getSearchOutIndeks(L, n, x));
//        System.out.println("data " + app.getSearchInBoolean(L, n, x));
//        System.out.println("data ada di indeks ke " + app.getSearchInIndeks(L, n, x));
//        System.out.println(" indeks = " + app.getSearchSentinel(L, n, x));
    }
}
