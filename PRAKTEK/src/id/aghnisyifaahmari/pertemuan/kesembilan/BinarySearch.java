package id.aghnisyifaahmari.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class BinarySearch {

    public int getBinarySearch(int[] L, int n, int x) {
        int i = 0, j, k = 0;
        boolean ketemu = false;
        j = n;
        
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
           
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] < x) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int x, n = 6;
        int[] L = {13, 14, 15, 16, 21, 76};
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan nilai x = ");
        x = in.nextInt();
        BinarySearch app = new BinarySearch();
        System.out.println("ada di indeks ke " + app.getBinarySearch(L, n, x));
    }
}
