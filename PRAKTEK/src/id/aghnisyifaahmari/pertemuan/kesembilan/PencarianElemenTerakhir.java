package id.aghnisyifaahmari.pertemuan.kesembilan;
import java.util.Scanner;
/**
 *
 * @author LENOVO
 */
public class PencarianElemenTerakhir {

    public int getIndeks(int[] L, int n, int x) {
        int i = n - 1;
        Boolean ketemu = false;
        while ((i > -1) && (i <= n - 1) && (!ketemu)) {
            if (i <= n - 1) {
                System.out.println("Posisi ke- " + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i - 1;
                        }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int i, x, n = 6;
        int[] L = new int[]{13, 16, 14, 21, 76, 15};
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan nilai x = ");
        x = in.nextInt();
        PencarianElemenTerakhir app = new PencarianElemenTerakhir();
        System.out.println("indeks = " + app.getIndeks(L, n, x));
    }
}
