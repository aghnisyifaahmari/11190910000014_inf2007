package id.aghnisyifaahmari.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class NilaiUjian {

    public static void main(String[] args) {
        int nilai;
        char indeks;
        Scanner in = new Scanner(System.in);
        nilai = in.nextInt();
        if (nilai >= 80) {
            indeks = 'A';
        } else if ((nilai >= 70) && (nilai < 80)) {
            indeks = 'B';
        } else if ((nilai >= 55) && (nilai < 70)) {
            indeks = 'C';
        } else if ((nilai >= 40) && (nilai < 50)) {
            indeks = 'D';
        } else {
            indeks = 'E';
        }
        System.out.println(indeks);
    }
}
