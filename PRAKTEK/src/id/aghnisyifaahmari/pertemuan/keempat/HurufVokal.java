package id.aghnisyifaahmari.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class HurufVokal {

    public static void main(String[] args) {
        char huruf;
        Scanner in = new Scanner(System.in);
        huruf = in.next().charAt(0);

        if (huruf == 'a' || huruf == 'i' || huruf == 'u' || huruf == 'e' || huruf == 'o') {
            System.out.println(huruf + " adalah huruf vokal");

        } else {
            System.out.println("bukan huruf vokal");
        }
    }
}
