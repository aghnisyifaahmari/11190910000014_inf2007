package id.aghnisyifaahmari.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author LENOVO
 */
public class DiskonHarga {
    public static void main(String[] args) {
        int diskon, totalBelanja, hargaAkhir;
        Scanner in = new Scanner(System.in);
        totalBelanja = in.nextInt();

        if (totalBelanja > 120000){
            diskon = totalBelanja*7/100;
            hargaAkhir = totalBelanja - diskon;
            System.out.println("diskon yang di dapatkan adalah "+ diskon);
            System.out.println("harga setelah diskon adalah "+ hargaAkhir);
        }else {
            System.out.println("anda tidak mendapatkan diskon");
        }
    }
}
