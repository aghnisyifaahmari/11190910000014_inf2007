package id.aghnisyifaahmari.pertemuan.keempat;
import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class KonversiAngkaKeTeks {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int angka;
        angka = in.nextInt();
        switch (angka) {
            case 1:
                System.out.println("pertama");
                break;
            case 2:
                System.out.println("kedua");
                break;
            case 3:
                System.out.println("ketiga");
                break;
            case 4:
                System.out.println("keempat");
                break;
            default:
                System.out.println("angka yang dimasukan salah");
        }
    }
}
