package id.aghnisyifaahmari.pertemuan.keempat;
import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class UrutanBilangan {
    public static void main(String[] args) {
        int A, B, C;
        Scanner in = new Scanner(System.in);
        A = in.nextInt();
        B = in.nextInt();
        C = in.nextInt();
        if ((A <= B) && (B <= C)) {
            System.out.println(A + "\n" + B + "\n" + C + "\n");
        } else {
            if ((A <= C) && (C <= B)) {
                System.out.println(A + "\n" + C + "\n" + B + "\n");
            } else if ((B <= A) && (A <= C)) {
                System.out.println(B + "\n" + A + "\n" + C + "\n");
            } else if ((B <= C) && (C <= A)) {
                System.out.println(B + "\n" + C + "\n" + A + "\n");
            } else if ((C <= A) && (A <= B)) {
                System.out.println(C + "\n" + A + "\n" + B + "\n");
            } else {
                System.out.println(C + "\n" + B + "\n" + A + "\n");
            }
        }
    }
}
