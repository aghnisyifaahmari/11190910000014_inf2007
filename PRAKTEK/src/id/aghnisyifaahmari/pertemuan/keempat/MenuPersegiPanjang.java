package id.aghnisyifaahmari.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class MenuPersegiPanjang {

    public static void main(String[] args) {
       int noMenu;
        double panjang, lebar;
        double luas, keliling, diagonal;
        Scanner in = new Scanner(System.in);
        System.out.println("Menu empat persegi panjang");
        System.out.println("1. Menghitung luas");
        System.out.println("2. Menghitung keliling ");
        System.out.println("3. Menghitung panjang diagonal");
        System.out.println("4. Keluar Program");
        System.out.println("Masukan pilihan anda (1/2/3/4)?");

        noMenu = in.nextInt();
        panjang = in.nextDouble();
        lebar = in.nextDouble();
        
        switch (noMenu) {
            case 1:
                luas = (panjang * lebar);
                System.out.println("luas persegi panjang adalah " + luas);
                break;
            case 2:
                keliling = (2 * (panjang + lebar));
                System.out.println("keliling persegi panjang adalah " + keliling);
                break;
            case 3:
                diagonal = ((panjang * panjang) + (lebar * lebar));
                System.out.println("panjang diagonal");
                break;
            case 4:
                System.out.println("keluar program...sampai jumpa");
                break;
        }

    }
}
