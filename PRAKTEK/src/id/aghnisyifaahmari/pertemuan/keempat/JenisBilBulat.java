package id.aghnisyifaahmari.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class JenisBilBulat {

    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        bilangan = in.nextInt();

        if (bilangan > 0) {
            System.out.println(bilangan + " adalah bilangan positif");
        } else {
            if (bilangan < 0) {
                System.out.println(bilangan + " adalah bilangan negatif");
            } else {
                if (bilangan == 0) {
                    System.out.println(bilangan + " adalah nol");
                }
            }
        }
    }
}
