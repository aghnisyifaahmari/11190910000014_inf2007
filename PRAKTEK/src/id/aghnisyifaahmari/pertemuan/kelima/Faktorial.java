package id.aghnisyifaahmari.pertemuan.kelima;
import java.util.Scanner;
/**
 *
 * @author LENOVO
 */
public class Faktorial {
    public static void main(String[] args) {
        Scanner in = new Scanner (System.in);
        int n, fak, i;
        n = in.nextInt();
        fak = 1;
        for (i = 1; i <= n; i++){
            fak = fak * i;
        }
        System.out.println(fak);
    }
}
