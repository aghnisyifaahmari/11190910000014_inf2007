package id.aghnisyifaahmari.pertemuan.kesebelas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author LENOVO
 */
public class TulisArsipBilangan {

    public static void main(String[] args) {
        int i, n;
        try {
            PrintWriter app = new PrintWriter(new FileOutputStream("D:\\SequentialFile\\app.txt"));
            Scanner in = new Scanner(System.in);
            System.out.println("masukan nilai n : ");
            n = in.nextInt();
            for (i = 1; i <= n; i++) {
                app.println(i);
             
            }
            app.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());

        }
    }
}
