package id.aghnisyifaahmari.pertemuan.kedua;

/**
 *
 * @author LENOVO
 */
public class Invest {
    public static void main(String[] arguments) {
        float total = 14000;
        System.out.println("Original investemet: $" + total);
        
        // Inceases by 40 percent the first year
        total = total +(total * .4F);
        System.out.println("After one years; $" + total);
        
        // Loses $1,500 the second year
        total = total + total - 1500F;
        System.out.println("After two years: $" + total);
        
        // Increases by 17 percent the third year
        total = total + (total * .12F);
        System.out.println("After third years: $" + total);
    } 
}
