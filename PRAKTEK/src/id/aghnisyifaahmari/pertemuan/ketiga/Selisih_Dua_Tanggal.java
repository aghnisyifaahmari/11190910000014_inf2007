package id.aghnisyifaahmari.pertemuan.ketiga;
import java.util.Scanner;
/**
 *
 * @author LENOVO
 */
public class Selisih_Dua_Tanggal {
    public static void main(String[] args) {
        int tanggal1, bulan1, tahun1, tanggal2, bulan2, tahun2;
        int tanggal3, bulan3, tahun3, selisih;
        Scanner in = new Scanner(System.in);
        System.out.print("tanggal 1 : ");
        tanggal1 = in.nextInt();
        System.out.print("bulan1: ");
        bulan1 = in.nextInt();
        System.out.print("tahun1 : ");
        tahun1 = in.nextInt();
        System.out.print("tanggal2 : ");
        tanggal2 = in.nextInt();
        System.out.print("bulan2 : ");
        bulan2 = in.nextInt();
        System.out.print("tahun2 : ");
        tahun2 = in.nextInt();
        
        selisih = (tahun2 - tahun1)*365 + (bulan2 - bulan1)*30 + (tanggal2 - tanggal1);
        tahun3 = selisih / 365;
        bulan3 = (selisih % 365)/30;
        tanggal3 = (selisih % 365)% 30;
        System.out.println("selisih : ");
        System.out.print(tahun3 + " tahun " + bulan3 +" bulan " +tanggal3 + " tanggal");  
    }
}
